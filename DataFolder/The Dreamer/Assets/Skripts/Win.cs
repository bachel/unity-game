﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Win : MonoBehaviour {
    // Gleich wie Transport Skript nur das hier der Sieg angezeigt wird und anstatt den Character an eine neue Position zu setzen rückkehr ins Hauptmenü
    public GameObject Portal;
    public Text ButtonPush;
    private bool intrigger = false;

    // Update is called once per frame
    void Update()
    {
        if (intrigger)
        {
            if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.JoystickButton2) || Input.GetKey(KeyCode.JoystickButton0))
            {

                
                ButtonPush.text = "Entkommen";
                Destroy(Portal);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                SceneManager.LoadScene("MainMenues");

            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        ButtonPush.text = "Drücke Q oder X";
        intrigger = true;

    }

    void OnTriggerExit(Collider other)
    {
        ButtonPush.text = "";
        intrigger = false;

    }
}
