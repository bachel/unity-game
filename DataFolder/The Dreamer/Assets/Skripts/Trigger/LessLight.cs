﻿using UnityEngine;
using System.Collections;

public class LessLight : MonoBehaviour {
    public float MinLicht = 1.0f;
    public Light flash;
    public GameObject item;
    public AudioSource platz;

    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            
            if(flash.intensity >= MinLicht)
            {
                flash.intensity -= 0.5f;
                flash.range -= 5;
            }

            Destroy(item);
            platz.Play();
        }
    }
}
