﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trank : MonoBehaviour {
    public GameObject item; // Object wesen Name ausgelesen wird 
    private string ObjectName;


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ObjectName = gameObject.transform.name; // Name des Gameobject in String umwandeln
            GameObject.Find("Display").SendMessage("ZeitItem", ObjectName); // Übergabe des Objektname an das Timer script
            Destroy(item); // GameObject zerstören
        }
    }
}
