﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoreSpeed : MonoBehaviour { // Gleich wie lessspeed nur das hier speed erhöht wird 

    public float MaxSpeed = 20f;
    public float NormalSpeed = 10f;
    public float Dauer = 10f;
    public GameObject item;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller;
    

    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {

            controller.m_WalkSpeed = MaxSpeed;
            Renderer[] rs = item.GetComponentsInChildren<Renderer>();
            foreach (Renderer r in rs)
            {
                r.enabled = false;
            }
                
            StartCoroutine("SpeedZurueckSetzen", Dauer);

        }

    }
    IEnumerator SpeedZurueckSetzen(float warten)
    {

        yield return new WaitForSeconds(warten);
        controller.m_WalkSpeed = NormalSpeed;
        Destroy(item);

    }
}
