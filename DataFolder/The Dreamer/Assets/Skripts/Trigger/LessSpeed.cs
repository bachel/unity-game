﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LessSpeed : MonoBehaviour {

    public float MaxSpeed = 1f; // Target Geschwindikeit Charakter
    public float NormalSpeed = 0.5f; // Normale Geschwindikeit Charakter
    public float Dauer = 10f; // Dauer wielange der Effekt anhalten soll
    public GameObject item; // Gameobject das den Effekt auslösen soll
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController controller; // FPS


    void OnTriggerEnter(Collider other) // Ausgelöst sobald trigger
    {

        if (other.CompareTag("Player"))
        {
            controller.m_WalkSpeed = MaxSpeed; // Fps laufgeschwindikeit anpassen 
            Renderer[] rs = item.GetComponentsInChildren<Renderer>(); // Gameobject und seine Kinder ausblenden und nicht zerstören das Waitforsecondes noch geht 
            foreach (Renderer r in rs)
            {
                r.enabled = false;
            }

            StartCoroutine("SpeedZurueckSetzen", Dauer); // Starten des IEnumerator und übergabe der Dauer 

        }

    }
    IEnumerator SpeedZurueckSetzen(float warten)
    {

        yield return new WaitForSeconds(warten);
        controller.m_WalkSpeed = NormalSpeed; // Geschwindikeit des Fps zurücksetzen
        Destroy(item); // Object entgültig zerstören

    }
}
