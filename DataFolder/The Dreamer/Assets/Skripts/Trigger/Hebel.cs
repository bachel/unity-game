﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hebel : MonoBehaviour {
    
    public GameObject stein; // Objekt das Zerstört werden soll
    private bool intrigger = false ; //Boolen um zu checken ob sich Charakter inerhalb des Triggers befindet
    public Text ButtonPush; // Text wiedergabe welcher knopf gedrückt werden muss 

    // Update is called once per frame
    void Update()
    {
        if (intrigger)
        {
            if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.JoystickButton2) || Input.GetKey(KeyCode.JoystickButton0)) // Warten für Tastendruck
            {

                Destroy(stein); // Zerstören
                ButtonPush.text = "Weg Frei !";

            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        ButtonPush.text = "Drücke Q oder X"; // Text der innerhalb des Trigger wiedergegeben wird 
        intrigger = true;

        }

    void OnTriggerExit(Collider other)
    {
        ButtonPush.text = "";
        intrigger = false;

    }






}
