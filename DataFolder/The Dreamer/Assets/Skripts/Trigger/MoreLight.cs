﻿using UnityEngine;
using System.Collections;

public class MoreLight : MonoBehaviour { // gleich wie lessspeed nur das hier das ambientlight angesprochen wird 
    public float MaxLicht = 1f;
    public float NormalLicht = 0.5f;
    public float Dauer = 10f;
    public GameObject item;
    public ParticleSystem fire;





    void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            RenderSettings.ambientIntensity = MaxLicht;
            item.gameObject.GetComponent<Renderer>().enabled = false;
            Destroy(fire);

            StartCoroutine("LichtZurueckSetzen", Dauer);
            
        }
            
    }
    IEnumerator LichtZurueckSetzen(float warten)
    {

        yield return new WaitForSeconds(warten);
        RenderSettings.ambientIntensity = NormalLicht;
        Destroy(item);

    }


}
