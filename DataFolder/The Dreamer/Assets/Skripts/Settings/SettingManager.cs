﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

public class Setting
{
    public bool vollbild;
    public int texturQuali;
    public int screenResIndex;
    public float volume;
    public float music;
    public float sfx;
    public int zeit;
    public int item;

}

public class SettingManager : MonoBehaviour {
    public Toggle Vollbild;
    public Dropdown Auflousung;
    public Dropdown Texturen;
    public Dropdown Zeit;
    public Dropdown Items;
    public Slider Volume;
    public Slider SFX;
    public Slider Music;
    public Button Apply;

    public Resolution[] auflousungen;
    public Setting einstellung;

     void Awake()
    {
        
        einstellung = new Setting();

        Vollbild.onValueChanged.AddListener(delegate { OnFullscreenToggle(); });
        Auflousung.onValueChanged.AddListener(delegate { OnResChange(); });
        Texturen.onValueChanged.AddListener(delegate { OnTextureChange(); });
        Zeit.onValueChanged.AddListener(delegate { OnZeitChange(); });
        Items.onValueChanged.AddListener(delegate { OnItemsChange(); });
        Volume.onValueChanged.AddListener(delegate { OnVolumeChange(); });
        SFX.onValueChanged.AddListener(delegate { OnSfxChange(); });
        Music.onValueChanged.AddListener(delegate { OnMusicChange(); });
        Apply.onClick.AddListener(delegate { OnApplyClick(); });


        auflousungen = Screen.resolutions;
        foreach(Resolution resolution in auflousungen)
        {
            Auflousung.options.Add(new Dropdown.OptionData(resolution.ToString()));
        }

        OnLoadSettings();

    }

    public void OnFullscreenToggle()
    {
        einstellung.vollbild = Screen.fullScreen = Vollbild.isOn;
    }
    public void OnResChange()
    {
        Screen.SetResolution(auflousungen[Auflousung.value].width, auflousungen[Auflousung.value].height, Screen.fullScreen);
        einstellung.screenResIndex = Auflousung.value;
    }

    public void OnTextureChange()
    {
       QualitySettings.masterTextureLimit = einstellung.texturQuali = Texturen.value;
    }

    public void OnZeitChange()
    {
        einstellung.zeit = Zeit.value;
    }

    public void OnItemsChange()
    {
        einstellung.item = Items.value;
    }

    public void OnVolumeChange()
    {
        AudioListener.volume = einstellung.volume = Volume.value;
    }

    public void OnSfxChange()
    {
        einstellung.sfx = SFX.value;
    }

    public void OnMusicChange()
    {
        einstellung.music = Music.value;
    }

    public void OnApplyClick()
    {
        OnApplySettings();
    }

    public void OnApplySettings()
    {
        string jsonData = JsonUtility.ToJson(einstellung, true);
        File.WriteAllText(Application.persistentDataPath + "/test.json", jsonData);
    }

    public void OnLoadSettings()
    {
        einstellung = JsonUtility.FromJson<Setting>(File.ReadAllText(Application.persistentDataPath + "/test.json"));
        Volume.value = einstellung.volume;
        Texturen.value = einstellung.texturQuali;
        Auflousung.value = einstellung.screenResIndex;
        Vollbild.isOn = einstellung.vollbild;
        Screen.fullScreen = einstellung.vollbild;
        SFX.value = einstellung.sfx;
        Music.value = einstellung.music;
        Zeit.value = einstellung.zeit;
        Items.value = einstellung.item;

        Auflousung.RefreshShownValue();
    }







}
