﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InGameUI : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape) || Input.GetKey(KeyCode.E)) // Listen ob Escape oder E gedrückt wird
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("MainMenues"); // Laden des Hauptmenüs
        }
	}
}
