﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Transport : MonoBehaviour {
    public Transform PortalPoint; // Ziel des Characters
    public GameObject Player; // Fps
    public GameObject Portal; // Eigentliches Portal
    public Text ButtonPush; // Textwiedergabe
    private bool intrigger = false; // Ob in Trigger

    // Update is called once per frame
    void Update()
    {
        if (intrigger)
        {
            if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.JoystickButton2) || Input.GetKey(KeyCode.JoystickButton0))
            {

                Player.transform.position = PortalPoint.position; // Sendet Fps an die Stelle PortalPoint Objects
                ButtonPush.text = "";
                Destroy(Portal);

            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        ButtonPush.text = "Drücke Q oder X"; // Wie bei den anderen 
        intrigger = true;

    }

    void OnTriggerExit(Collider other)
    {
        ButtonPush.text = "";
        intrigger = false;

    }
}
