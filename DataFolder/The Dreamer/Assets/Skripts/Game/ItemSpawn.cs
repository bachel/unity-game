﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour {
    public Transform[] SpawnPoints; // Array der Spawn Points
    public GameObject[] Items; // Array der Items die gespawnt werden sollen
    public float spawntime = 0.5f; // nach welcher zeitspanne gespawnd werden soll
	// Use this for initialization
	void Start () {
        Invoke("SpawnItems", spawntime); // auslösen des spawnens
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void SpawnItems()
    {
        int SpawnIndex = 0; // Start Punkt des Spawn Arrays
        for (int i = 0; i < SpawnPoints.Length; i++)
        {
            SpawnIndex++; // +1
            int ItemIndex = Random.Range(0, Items.Length); // Random Auswaahl welches Item gespawnd wird 

            Instantiate(Items[ItemIndex], SpawnPoints[SpawnIndex].position, SpawnPoints[SpawnIndex].rotation); // Random Item an Position von SpawnPoint setzen 
        }
        
    }
}
