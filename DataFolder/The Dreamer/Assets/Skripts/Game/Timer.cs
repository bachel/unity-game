﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {
    public Text timerText; // Timer Anzeige
    private float Zeit = 3f; // Zeit um level zu bewältigen
    private float ZeitinMinuten; // StartZeit in Sekunden
    public Text GameOver; // GameOver und Win Anzeige
    private bool Stop = false; // Timer läuft 
    private float time; // System Zeit

    // Use this for initialization
    void Start () {
         ZeitinMinuten = Zeit * 60; // Zeit in Sekunden umrechnen auch wenn wir Minuten geschrieben haben 
	}
	
	// Update is called once per frame
	void Update () {
        if (Stop) // Checken ob Timer Abgelaufen
        {
            return;
        }
        else
        {
            time = ZeitinMinuten - Time.time; // Berechnen wieviel Zeit seid Start vergangen ist 
            int Minuten = ((int)time / 60); // Sekunden in Minuten um Minuten anzuzeigen
            float Sekunden = Mathf.RoundToInt(time % 60); // Sekunden auf 2 Stellen runden

            string minuten = Minuten.ToString(); // Minuten in String umwandeln um Sie als Text wiederzugeben
            string sekunden = Sekunden.ToString(); // "" für sekunden

            if (Minuten < 10)
            {
                minuten = "0" + Minuten.ToString();
            }                                               // Falls Sekunden oder Minuten unter 10 fallen eine 0 davor setzen das es gut aussieht
            if (Sekunden < 10)
            {
                sekunden = "0" + Sekunden.ToString();
            }
            if (Minuten < 1)
            {
                timerText.color = Color.yellow; // Textfarbe auf Gelb setzen sobald Timer unter eine Minute fällt
            }
            if (Minuten == 0 && Sekunden == 0 ) // was passiert wenn Zeit abläuft 
            {
                Stop = true; // Läuft nicht mehr
                GameOver.text = "Game Over"; // Game Over auf dem Bildschirm darstellen
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                SceneManager.LoadScene("MainMenues"); // Laden des Hauptmenüs
            }

            timerText.text = minuten + ":" + sekunden; // Minuten und Sekunden wiedergeben

        }
        
    



    }
    public void ZeitItem(string Name) // Hier werden die Objektnamen der Tränke angenommen und je nach Trank Zeit hinzugefügt oder angezogen 
    {
        
        if (Name == "trankeinsPrefab(Clone)")
        {
            ZeitinMinuten += 5f;
            
        }
        if (Name == "trankzweiPrefab(Clone)")
        {
            ZeitinMinuten += 10f;

        }
        if (Name == "trankdreiPrefab(Clone)")
        {
            ZeitinMinuten -= 5f;

        }
        if (Name == "trankvierPrefab(Clone)")
        {
            ZeitinMinuten -= 10f;

        }
    }
}
