﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;


public class MusikUi : MonoBehaviour {
    static bool AudioBegin = false;
    public Setting einstellung;
    
    void Awake()
    {
        if (!AudioBegin)
        {
            
            GetComponent<AudioSource>().Play();
            DontDestroyOnLoad(gameObject);
            AudioBegin = true;
            
        }
    }
    void Update()
    {
        if (Application.loadedLevelName == "LevelOne" || Application.loadedLevelName == "LevelTwo" || Application.loadedLevelName == "LevelThree" || Application.loadedLevelName == "Testlevel")
        {
            GetComponent<AudioSource>().Stop();
            AudioBegin = false;
        }
    }
}
