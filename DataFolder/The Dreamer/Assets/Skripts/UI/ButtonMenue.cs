﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonMenue : MonoBehaviour {

	public void MenuButton(string MenuName) // Auslesen der gedrückten Knöpfe (Wert der in Button eingetragen wird )
    {   
        if(MenuName == "Exit") // Sollte übergebener wert Exit sein Programm beenden ansonsten Scene mit jeweiligen Namen laden
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(MenuName);
        }
        
    }
    
}
